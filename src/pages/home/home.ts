import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { AlertController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  qrData = null;
  createdCode = null;
  scannedCode = null;
  secNum = null;
  sid = this.navParams.get("sid");
  balance = this.navParams.get("balance");
  
  constructor(public navCtrl: NavController,private http: HTTP, private alertCtrl: AlertController, private navParams:NavParams) {
    this.sid = this.navParams.get("sid");
    this.balance = this.navParams.get("balance");
  }
  ionViewDidEnter(){
 this.sid = this.navParams.get("sid");
    this.balance = this.navParams.get("balance");
  }
  ionViewWillEnter(){
this.sid = this.navParams.get("sid");
    this.balance = this.navParams.get("balance");
  }
  createCode() {
    if(!(this.qrData)){
      this.presentAlertForAmount();
      return;
    }
    console.log(this.sid);
    console.log(this.balance);
    var jSONValue = "{\"amount\": "+this.qrData+",\"sid\": "+this.sid+"}"; 
    this.createdCode = jSONValue;
  }
  submitSecret(){
    if(!(this.qrData)){
      this.presentAlertForAmount();
      return;
    }
    if(!(this.secNum)){
      this.presentAlertForSecret();
       return;
    }
    this.http.post('http://18.217.229.46/user.php', {"action": "secret_transaction", "sec": this.secNum, "sid": this.sid, "amount": +this.qrData}, {})
  .then(data => {
     let alert = this.alertCtrl.create({
    title: 'Done',
    subTitle: 'the transaction was successful',
    buttons: ['Dismiss']
  });
  alert.present();
  })
  .catch(error => {

    console.log(error.status);
    console.log(error.error); // error message as string
    console.log(error.headers);

  });


}
presentAlertForAmount() {
  let alert = this.alertCtrl.create({
    title: 'No amount',
    subTitle: 'please insert amount',
    buttons: ['Dismiss']
  });
  alert.present();
  }
  presentAlertForSecret() {
    let alert = this.alertCtrl.create({
      title: 'No Secret Entered',
      subTitle: 'please insert your Secret number',
      buttons: ['Dismiss']
    });
    alert.present();
    }

}
