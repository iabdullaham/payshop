import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  phone:string = "";
  Password:string = "";
  

  constructor(public navCtrl: NavController, public navParams: NavParams,private http: HTTP) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login(){
    console.log(this.phone);
    console.log(this.Password);
    this.http.post('http://18.217.229.46/user.php', {"action": "login", "phone": this.phone,"password": this.Password}, {})
  .then(data => {
    console.log('wow');
    var d = JSON.parse(data.data);
    console.log(d.data);
    console.log(d.data.id);
    this.navCtrl.push(HomePage,{"sid": d.data.id, "balance": d.data.BALANCE},{});
  })
  .catch(error => {

    console.log(error.status);
    console.log(error.error); // error message as string
    console.log(error.headers);

  });


  }

}
